# M223: Punchclock
Das ist die Abschlussapplikation von mir, Linden koppejan, für das modul 223.

## Wie wird das Projekt Gestartet?

Um das Projekt zum Laufen zu bringen, wird in der VSC Console, dem Terminal folgender befehl ausgeführt: `./gradlew bootRun`. Zu versichern ist das JDK 11 oder höher installiert ist. 
Nach dem das Projekt erfolgreich kompiliert hat, kann es über folgende URL Aubgerufen werden: `http://localhost:8081/sign-up.html`.


## Was kann PunchClock?

An der Applikation kann man sich an einem bestehenden oder bereits vorhandenen Konto anmelden. Nachdem man sich eingelogt hat, wird es dem Benutzer ermöglicht, Arbeitszeiten zu erfassen, diese mit einem Kommentar zu versehen, bearbeiten oder gar zu löschen.