package ch.zli.m223.punchclock.domain;
import javax.persistence.*;




@Entity
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
   
    @Column(nullable = false)     
    private String type;

    @Column(nullable = false)
    private Long proj_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }    

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    } 

    public Long getProject_id() {
        return proj_id;
    }

    public void setProject_id(Long proj_id) {
        this.proj_id = proj_id;
    }   
}
