package ch.zli.m223.punchclock.domain;
import javax.persistence.*;


import java.util.List;

@Entity
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
   
    @Column(nullable = false)   
    private String projectName;

    @OneToMany
    private List<Activity> activities;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
    
    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }
}
