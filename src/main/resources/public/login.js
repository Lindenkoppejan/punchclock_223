const URL = 'http://localhost:8081';
let users = [];
let entries = [];

const login = async (func) => {
    func.preventDefault(); 
    console.log("ichsollte einloggen");       
    const formData = new FormData(func.target);
    const loginData = {};
    loginData['username'] = formData.get('loginName');
    loginData['password'] = formData.get('loginPassword');

    const response = await fetch(`${URL}/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(loginData),
    });
    let mytoken = await response.headers.get('Authorization');    

    if (mytoken != null) {
       await localStorage.setItem('token', mytoken);
       window.location.replace('/index.html');
    }
};

document.addEventListener('DOMContentLoaded', function(){    
    const createLogin = document.querySelector('#login');
    createLogin.addEventListener('submit',login);
});